#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
import torch
import json
from cycler import cycler


def get_perclass_log(pc_val, label_dict):
    """
    TODO Redo doc
    Returns the values per class with numeric labels replaced by their names

    :param pc_val: tuple, pc_val[0] represents confusion matrix values
    pc_val[1] is the relevant classes for indices in pc_val[0]
    :param label_dict: dict, the corresponding name of a numeric label
    :returns: dict, the values per class with their string labels

    """
    reverse_dict = {i: label for label, i in label_dict.items()}
    l = {}
    for v, i in zip(pc_val, reverse_dict.keys()):
        l[reverse_dict[i]] = v
    return l


def get_loss_line_coeffs(losses):
    """
    Returns the slope of the line representing the losses' evolution

    :param losses: array_like of floats
    :returns: float
    """
    x = np.arange(len(losses))
    mean_x = np.sum(x) / len(losses)
    mean_losses = np.sum(losses) / len(losses)
    return np.sum((x - mean_x) * (losses - mean_losses)) / np.sum((x - mean_x) ** 2)


def get_class_pred(preds):
    """
    Get predicted labels from a neural network output,
    i.e. get index of the highest value

    :param preds: array-like of array-like of floats, values at the last layer
    :returns: array-like of ints
    """
    preds_labels = torch.log_softmax(preds, dim=1)
    _, preds_labels = torch.max(preds_labels, dim=1)
    return preds_labels


def get_acc(preds_classes, labels):
    """
    Computes accuracy of a neural network predicted labels

    :param preds_classes: array-like of ints, classes predicted
    :param labels: array-like of ints, truth labels
    :returns: int, the percentage of correct predictions
    """
    accuracy = (preds_classes == labels).sum().item() / labels.shape[0] * 100.0
    return accuracy


def save_figure(path, high_res):
    if high_res:
        plt.savefig(path + ".svg", format="svg")
    else:
        plt.savefig(path + ".png")


def generic_plot(
    steps,
    data,
    classes,
    path_save,
    title,
    xlabel,
    ylabel,
    ylim,
    has_title=True,
    has_legend=True,
    high_res=False,
):
    fig = plt.figure(figsize=(12, 8))
    plt.gca().set_prop_cycle(
        cycler(
            "color",
            [plt.cm.gist_ncar(i) for i in np.linspace(0, 0.9, max(len(classes), 15))],
        )
    )
    # print([plt.cm.gist_ncar(i) for i in np.linspace(0, 0.9, max(len(classes), 15))])
    for i, _ in enumerate(list(classes)):
        plt.plot(steps, data[i])
    if has_legend:
        plt.legend(
            classes,
            bbox_to_anchor=(1.02, 1),
            loc="upper left",
            borderaxespad=0,
            fontsize="xx-large",
        )
    else:
        plt.legend().set_visible(False)
    if has_title:
        plt.title(title, fontsize=20, pad=10)
    plt.xlabel(xlabel, fontsize="xx-large")
    plt.ylabel(ylabel, fontsize="xx-large", labelpad=10)
    plt.xticks(fontsize="xx-large")
    plt.yticks(fontsize="xx-large")
    plt.ylim(ylim)
    plt.tight_layout()
    save_figure(path_save, high_res)
    plt.close(fig)


def perf_plot(epochs, data, path_save, title, **kwargs):
    fig = plt.figure(figsize=(15, 10))
    plt.plot(epochs, data)
    bests = max(data), epochs[np.argmax(data)]
    if title == "Loss":
        bests = min(data), epochs[np.argmin(data)]
    plt.title("{0}\nBest {0} = {1:.4f} (epoch {2})".format(title, bests[0], bests[1]))
    plt.xlabel("Epochs")
    plt.tight_layout()
    save_figure(path_save, kwargs.get("high_res", False))
    plt.close(fig)


def perclass_perf_plot(
    epochs, data, path_save, title, macro_vals, micro_vals, **kwargs
):
    fig = plt.figure(figsize=(15, 10))
    plt.gca().set_prop_cycle(
        cycler(
            "color",
            [plt.cm.nipy_spectral(i) for i in np.linspace(0, 0.9, len(data[0].keys()))],
        )
    )
    multip = 100
    bests = (
        max(macro_vals),
        epochs[np.argmax(macro_vals)],
        max(micro_vals),
        epochs[np.argmax(micro_vals)],
    )
    if title in ["FalseAlarmCost", "MissCost", "AttackCost"]:
        multip = 1
        bests = (
            min(macro_vals),
            epochs[np.argmin(macro_vals)],
            min(micro_vals),
            epochs[np.argmin(micro_vals)],
        )
    for label in list(data[0].keys()):
        plt.plot(epochs, [data[i][label] * multip for i in range(len(data))])
    plt.legend(
        data[0].keys(),
        bbox_to_anchor=(1.02, 1),
        loc="upper left",
        borderaxespad=0,
    )
    plt.rc("legend", fontsize="medium")
    plt.title(
        "{0}\nBest macro {0} = {1:.4f} (epoch {2})\nBest micro {0} = {3:.4f} (epoch {4})".format(
            title, bests[0], bests[1], bests[2], bests[3]
        )
    )
    plt.xlabel("Epochs")
    plt.tight_layout()
    save_figure(path_save, kwargs.get("high_res", False))
    plt.close(fig)


def save_cfm(accs, label_dict, fmt=".2f", figure_title=None):
    cfm_labels = label_dict.keys()
    fig_size = (6 * np.sqrt(len(cfm_labels)), 5 * np.sqrt(len(cfm_labels)))
    plt.figure(figsize=fig_size)
    if len(cfm_labels) == 2:
        fmt = ".3f"
    cfm = sn.heatmap(
        accs,
        annot=True,
        fmt=fmt,
        annot_kws={"fontsize": 75 / np.sqrt(len(cfm_labels))},
        linewidths=0.1,
        cmap=sn.color_palette("Blues", as_cmap=True),
    )
    cfm.set_xticks(np.arange(len(cfm_labels)) + 0.5)
    cfm.set_yticks(np.arange(len(cfm_labels)) + 0.5)
    cfm.set_xticklabels(
        cfm_labels, rotation=45, ha="right", rotation_mode="anchor", fontsize="x-large"
    )
    cfm.set_yticklabels(cfm_labels, rotation="horizontal", fontsize="x-large")
    cfm.set_ylabel("Ground truth", fontsize="xx-large", fontweight="bold")
    cfm.set_xlabel("Predicted", fontsize="xx-large", fontweight="bold")
    fig_title = "" if figure_title is None else figure_title + "\n"
    cfm.set_title(
        "{}Confusion Matrix".format(fig_title),
        fontsize="xx-large",
        fontweight="bold",
        pad=15,
    )
    plt.tight_layout()
    plt.close(cfm.figure)
    return cfm.figure


def save_hist(macro_val, micro_val, pc_val, name, figure_title=None):
    hist_labels = pc_val.keys()
    fig = plt.figure(
        figsize=(5 * np.sqrt(len(hist_labels)), 2 * np.sqrt(len(hist_labels)))
    )
    plt.bar(hist_labels, pc_val.values())
    plt.xticks(
        np.arange(len(hist_labels)),
        hist_labels,
        rotation=45,
        ha="right",
        rotation_mode="anchor",
    )
    plt.xlabel("Class", fontweight="bold")
    fig_title = "" if figure_title is None else figure_title + "\n"
    plt.title(
        "{0}Macro {1} = {2:.6f}\nMicro {1} = {3:.6f}".format(
            fig_title, name, macro_val, micro_val
        )
    )
    plt.ylim()
    plt.tight_layout()
    plt.close(fig)
    return fig


class MyJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)
