#!/usr/bin/env python3

import os
from aim.sdk.errors import MissingRunError
import dvc.api
import logging
import pickle
import explainers
import numpy as np
from aim import Run

from make_aim import make_aim

logger = logging.getLogger("dvc")
logger.setLevel(10)


def explain_model(params):
    dataset_name = params["dataset"]["name"]

    with open("processed_data/{}/dataset_info".format(dataset_name), "rb") as f:
        dataset_info = pickle.load(f)
    with open("processed_data/{}/train_data.npy".format(dataset_name), "rb") as f:
        train_data = np.load(f)
    with open("processed_data/{}/train_labels.npy".format(dataset_name), "rb") as f:
        train_labels = np.load(f)
    with open("processed_data/{}/test_data.npy".format(dataset_name), "rb") as f:
        test_data = np.load(f)
    with open("processed_data/{}/test_labels.npy".format(dataset_name), "rb") as f:
        test_labels = np.load(f)

    try:
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
        logger.info("Current run hash: {}".format(aim_runner.hash))
    except MissingRunError:
        make_aim(params)
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)

    if os.environ.get("DVC_EXP_NAME", "unknown") not in aim_runner["dvc_exp"]:
        aim_runner["dvc_exp"].append(os.environ.get("DVC_EXP_NAME", "unknown"))

    with open("untrained_model.pkl", "rb") as f:
        model = pickle.load(f)

    # Needs X_train, X_test, y_test, feature names, class_names, model,

    model.load_model("save_experiments/trained_model")
    explainers.make_explanations(
        params["explanation"]["algo"] + "_explanation",
        train_data=train_data,
        train_labels=train_labels,
        test_data=test_data,
        test_labels=test_labels,
        label_dict=dataset_info["labels_dict"],
        feature_names=dataset_info["feature_names"],
        normalizer=dataset_info["normalizer"],
        learner=model,
        aim_runner=aim_runner,
        num_features=params["explanation"].get("num_features", 5),
        num_explan=params["explanation"].get("num_explan", 3),
        classification_result=params["explanation"].get("classification_result", None),
        checks=params["explanation"].get("checks", {}),
    )


if __name__ == "__main__":
    params = dvc.api.params_show(stages=["explain_model", "make_aim"])

    if params["explanation"]["algo"] is not None:
        explain_model(params)
    else:
        logger.info("No explanation algorithm specified")
