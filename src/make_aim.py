#!/usr/bin/env python3

import os
from time import sleep
import dvc.api
import logging
from aim import Run

logger = logging.getLogger("dvc")
logger.setLevel(10)


def make_aim(params):
    dataset_name = params["dataset"]["name"]

    model_info = params["model"]["name"]

    aim_runner = Run(
        experiment=f"{dataset_name} - {model_info}"
        if params.get("experiment_name", None) is None
        else f"{params['experiment_name']}",
        system_tracking_interval=None,
    )
    sleep(1)
    with open("current_aim_run", "w") as f:
        f.write(aim_runner.hash)

    logger.info(f"Current run hash: {aim_runner.hash}")

    aim_runner["dataset_params"] = params["dataset"]
    aim_runner["model_params"] = params["model"]
    aim_runner["dvc_exp"] = [os.environ.get("DVC_EXP_NAME", "unknown")]

    if not os.path.exists("before_train"):
        with open("before_train", "w") as f:
            f.write("empty")


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_aim")

    make_aim(params)
