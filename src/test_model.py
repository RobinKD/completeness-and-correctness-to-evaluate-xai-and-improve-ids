#!/usr/bin/env python3

import os
from aim.sdk.base_run import MissingRunError
import dvc.api
import logging
import pickle
import numpy as np
from aim import Run

from make_aim import make_aim

logger = logging.getLogger("dvc")
logger.setLevel(10)


def test_model(params):
    dataset_name = params["dataset"]["name"]

    with open("processed_data/{}/dataset_info".format(dataset_name), "rb") as f:
        dataset_info = pickle.load(f)
    with open("processed_data/{}/test_data.npy".format(dataset_name), "rb") as f:
        test_data = np.load(f)
    with open("processed_data/{}/test_labels.npy".format(dataset_name), "rb") as f:
        test_labels = np.load(f)

    test_cvss = None
    if params["dataset"]["parameters"]["with_cve"]:
        with open("processed_data/{}/test_cvss".format(dataset_name), "rb") as f:
            test_cvss = pickle.load(f)

    try:
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
        logger.info("Current run hash: {}".format(aim_runner.hash))
    except MissingRunError:
        make_aim(params)
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)

    if os.environ.get("DVC_EXP_NAME", "unknown") not in aim_runner["dvc_exp"]:
        aim_runner["dvc_exp"].append(os.environ.get("DVC_EXP_NAME", "unknown"))

    with open("untrained_model.pkl", "rb") as f:
        model = pickle.load(f)

    os.makedirs("save_experiments/test", exist_ok=True)
    model.load_model("save_experiments/trained_model")
    model.test(
        X=test_data,
        y=test_labels,
        label_dict=dataset_info["test_labels_dict"],
        aim_runner=aim_runner,
        test_cvss=test_cvss,
        dataset_info=dataset_info,
        image_format="svg",
        out_path="saved_results/{}".format(aim_runner.hash),
    )

    if (
        params["dataset"]["parameters"].get("cve_envs", False)
        and params["dataset"]["parameters"]["with_cve"]
    ):
        for i, n in enumerate(["High C, Low I, Low A", "High A, Low I, Low C"]):
            with open(
                "processed_data/{}/test_cvss_{}".format(dataset_name, i + 1), "rb"
            ) as f:
                test_cvss = pickle.load(f)
                model.test(
                    X=test_data,
                    y=test_labels,
                    label_dict=dataset_info["test_labels_dict"],
                    aim_runner=aim_runner,
                    test_cvss=test_cvss,
                    figure_title="Test CVSS env {}".format(n),
                    dataset_info=dataset_info,
                    image_format="tikz",
                    out_path="saved_results/{}".format(aim_runner.hash),
                )


if __name__ == "__main__":
    params = dvc.api.params_show(stages=["test_model", "make_aim"])

    test_model(params)
