#!/usr/bin/env python3

import datasets
import dvc.api
import logging

logger = logging.getLogger("dvc")
logger.setLevel(10)


def make_dataset(params):
    name = params["dataset"]["name"]
    dataset_params = params["dataset"]["parameters"]

    dataset = datasets.create_dataset(
        name.lower() + "_dataset", path=params["dataset"]["path"]
    )
    dataset.make_dataset(random_seed=params["random_seed"], **dataset_params)
    dataset.write_to_file(name, dataset_params.get("save_csv", False))


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_dataset")

    make_dataset(params)
