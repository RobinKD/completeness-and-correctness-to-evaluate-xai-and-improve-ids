#!/usr/bin/env python3

import pyplugs
import pickle
import numpy as np
import pandas as pd
from datasets.basic_dataset import BasicDataset

# TODO Remake tests


@pyplugs.register
class CICIDS2017Dataset(BasicDataset):
    """
    Aggregates the CICIDS2017 dataset
    ([from Kaggle](https://www.kaggle.com/cicdataset/cicids2017))
    from files present in a local directory, and perform relevant cleaning
    """

    def __init__(self, path, random_seed=43):
        super().__init__(path, random_seed)

    def clean(self, **kwargs):
        """
        Cleans dataset by removing instances with infinite and weird values < 0,
        except for features "Init_Win_bytes_forward", "Init_Win_bytes_backward"
        that contain too many of them
        """
        # self.explore()
        #: Sanitize weird char in web attack labels and remove unnecessary spaces
        self.data = self.data.replace("[^A-Za-z0-9 -]", "", regex=True).replace(
            "[ ]+", " ", regex=True
        )
        self.data[
            self.data.drop(
                ["Init_Win_bytes_forward", "Init_Win_bytes_backward", "Label"], axis=1
            )
            < 0
        ] = np.nan
        super().clean()
        if kwargs.get("with_cve", False):
            self.add_cvss(kwargs.get("cve_envs", False))
        # self.explore()

    def drop_labels(self):
        """
        Replaces string labels by integers and separates labels from data
        """
        self.data.loc[self.data["Label"] == "BENIGN", "Label"] = "normal"
        super().drop_labels()

    def train_test_split(self, train_prop=0.7, **kwargs):
        """
        Separates dataset into train / test with proportion according to
        parameter
        A bit faster than pytorch version

        :param train_prop: proportion of the data that becomes train
        """
        _, test_index = super().train_test_split(train_prop, **kwargs)
