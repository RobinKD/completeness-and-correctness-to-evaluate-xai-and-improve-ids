#!/usr/bin/env python3

import os
import pyplugs
import logging
import numpy as np
import pandas as pd
from datasets.basic_dataset import BasicDataset

logger = logging.getLogger("dvc")
logger.setLevel(10)


@pyplugs.register
class UNSWNB15Dataset(BasicDataset):
    """
    Aggregates the CICIDS2017 dataset
    ([from Kaggle](https://www.kaggle.com/cicdataset/cicids2017))
    from files present in a local directory, and perform relevant cleaning
    """

    def __init__(self, path, random_seed=43):
        super().__init__(path, random_seed)
        self.subcats = False

    def get_data(self):
        # pd.set_option("display.max_rows", None, "display.max_columns", None)
        filenames = [f for f in os.listdir(self.path) if ".csv in f"]
        self.feature_names = [f for f in filenames if "feature" in f]
        self.feature_names = pd.read_csv(
            self.path + "UNSW-NB15_features.csv", encoding="latin_1"
        )
        self.data = pd.concat(
            [
                pd.read_csv(
                    f,
                    encoding="latin_1",
                    names=self.feature_names["Name"],
                    low_memory=False,
                    # na_values=[" "]
                    # dtype=dtypes,
                )
                for f in [self.path + "UNSW-NB15_{}.csv".format(i) for i in range(1, 5)]
            ]
        )

        self.gt_info_path = self.path + "UNSW-NB15_GT.csv"

    def clean(self, **kwargs):
        """ """
        # self.explore()
        self.data = self.data.replace([np.inf, -np.inf, "", " "], np.nan)
        self.data[["state"]] = self.data[["state"]].replace(["-", "no"], "no_state")
        self.data[["service"]] = self.data[["service"]].replace("-", "no_service")
        self.data[["sport", "dsport"]] = self.data[["sport", "dsport"]].replace(
            "-", np.nan
        )
        self.data[["sport", "dsport"]] = self.data[["sport", "dsport"]].applymap(
            lambda x: int(x, 16)
            if type(x) == type("a") and "0x" in x
            else int(x)
            if type(x) == type("a")
            else x,
            na_action="ignore",
        )
        self.data[["ct_ftp_cmd"]] = self.data[["ct_ftp_cmd"]].applymap(
            lambda x: int(x), na_action="ignore"
        )
        self.data[["ct_flw_http_mthd", "is_ftp_login", "ct_ftp_cmd"]] = self.data[
            ["ct_flw_http_mthd", "is_ftp_login", "ct_ftp_cmd"]
        ].replace(np.nan, 0)
        self.data[self.data[["sport", "dsport"]] > 65536] = np.nan

        self.data[["attack_cat"]] = (
            self.data[["attack_cat"]]
            .replace("Backdoors", "Backdoor")
            .applymap(lambda x: x.strip().lower() if type(x) == type("") else x)
        )
        self.data[["Label"]] = self.data[["attack_cat"]].replace(np.nan, "normal")

        self.data = self.data.drop(
            ["srcip", "dstip", "Stime", "Ltime", "attack_cat"], axis=1, inplace=False
        )

        super().clean()

    def onehot_encoding(self, max_categories=9):
        system_ports = (0, 1023)
        registered_ports = (1024, 49151)
        dynamic_ports = (49152, 65536)

        self.data["sport_is_sys_port"] = (
            (self.data["sport"] >= system_ports[0])
            .mul(self.data["sport"] <= system_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_sys_port"] = (
            (self.data["dsport"] >= system_ports[0])
            .mul(self.data["dsport"] <= system_ports[1])
            .astype(np.uint8)
        )
        self.data["sport_is_reg_port"] = (
            (self.data["sport"] >= registered_ports[0])
            .mul(self.data["sport"] <= registered_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_reg_port"] = (
            (self.data["dsport"] >= registered_ports[0])
            .mul(self.data["dsport"] <= registered_ports[1])
            .astype(np.uint8)
        )
        self.data["sport_is_dyn_port"] = (
            (self.data["sport"] >= dynamic_ports[0])
            .mul(self.data["sport"] <= dynamic_ports[1])
            .astype(np.uint8)
        )
        self.data["dsport_is_dyn_port"] = (
            (self.data["dsport"] >= dynamic_ports[0])
            .mul(self.data["dsport"] <= dynamic_ports[1])
            .astype(np.uint8)
        )
        self.data = self.data.drop(["sport", "dsport"], axis=1, inplace=False)
        self.categorical_features = self.data.columns[:-1][
            self.data.iloc[:, :-1].nunique() <= max_categories
        ]
        logger.info(
            "The following features will be onehot encoded, because they have less than {} unique values:\n{}".format(
                max_categories + 1, self.categorical_features
            )
        )

        cols = (
            self.data.select_dtypes(exclude=np.number).columns.tolist()
            + self.categorical_features.tolist()
        )
        self.data = pd.get_dummies(self.data, columns=cols)
        self.nb_features = len(self.data.columns)
        self.feature_names = self.data.columns
