#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd
import torch
import pyplugs
import pickle
import logging
from sklearn.preprocessing import MinMaxScaler, RobustScaler, StandardScaler
from sklearn.model_selection import StratifiedShuffleSplit
from torch.utils.data import Dataset

logger = logging.getLogger("dvc")
logger.setLevel(10)


@pyplugs.register
class BasicDataset:
    """
    Basic dataset class
    Will be inherited by other dataset classes
    """

    def __init__(self, path, random_seed=43):
        self.random_seed = random_seed
        self.path = os.getcwd() + "/" + path

    def get_data(self):
        filenames = [self.path + f for f in os.listdir(self.path)]
        self.data = pd.concat([pd.read_csv(f) for f in filenames])

    def clean(self):
        """
        Cleans dataset by removing instances with infinite values
        """
        self.data = self.data.replace([np.inf, -np.inf], np.nan)
        logger.info(
            "Dropping {} instances with NaN".format(
                np.count_nonzero(self.data.isna().sum(axis=1))
            )
        )
        self.data = self.data.dropna(axis=0).reset_index(drop=True)

        # Find categorical features that have numeric values
        # Features that have a unique value are to be dropped
        uniq_val_cols = self.data.columns[:-1][(self.data.iloc[:, :-1].nunique() == 1)]
        if len(uniq_val_cols) > 0:
            self.data = self.data.drop(columns=uniq_val_cols)
            logger.info(
                "Dropped the following columns having a unique value:\n{}".format(
                    uniq_val_cols
                )
            )

    def drop_labels(self):
        """
        Replaces string labels by integers and separates labels from data
        """
        self.labels_dict = {
            "normal": 0,
            **dict(
                (x, i + 1)
                for i, x in enumerate(
                    [a for a in self.data["Label"].unique() if a != "normal"]
                )
            ),
        }
        self.labels = self.data["Label"].replace(self.labels_dict)
        self.data = self.data.drop(columns=["Label"], axis=1, inplace=False)
        self.nb_features = len(self.data.columns)
        self.feature_names = self.data.columns
        self.class_support = self.labels.value_counts().to_dict()
        self.nb_classes = len(np.unique(self.labels))

    def binary_labels(self):
        (
            self.labels[self.labels == self.labels_dict["normal"]],
            self.labels[self.labels != self.labels_dict["normal"]],
        ) = (0, 1)
        self.labels_dict = {"normal": 0, "attack": 1}

    def onehot_encoding(self, max_categories=9):
        # Features that have few possible values can be onehot encoded
        self.categorical_features = self.data.columns[:-1][
            (self.data.iloc[:, :-1].nunique() <= max_categories)
            & (self.data.iloc[:, :-1].nunique() > 2)
        ]
        logger.info(
            "The following features will be onehot encoded, because they have less than {} unique values:\n{}".format(
                max_categories + 1, self.categorical_features
            )
        )

        self.data = pd.get_dummies(self.data, columns=self.categorical_features)
        self.nb_features = len(self.data.columns)
        self.feature_names = self.data.columns

    def train_test_split(self, train_prop=0.7, **kwargs):
        """
        Separates dataset into train / test with proportion according to
        parameter
        A bit faster than pytorch version

        :param train_prop: proportion of the data that becomes train
        """
        sss = StratifiedShuffleSplit(
            n_splits=1, train_size=train_prop, random_state=self.random_seed
        )
        train_index, test_index = next(sss.split(self.data, self.labels))
        self.train_data = self.data.iloc[train_index]
        self.test_data = self.data.iloc[test_index]
        self.train_labels = self.labels.iloc[train_index]
        self.test_labels = self.labels.iloc[test_index]
        if kwargs.get("with_cve", False):
            self.train_cvss_scores = self.cvss_scores.iloc[train_index]
            self.test_cvss_scores = self.cvss_scores.iloc[test_index]

        return train_index, test_index

    def normalize_data(self, scaler="StandardScaler", **kwargs):
        """
        Use sklearn RobustScaler class
        """
        if scaler == "RobustScaler":
            quantile_range = kwargs.get("quantile_range", (1, 99))
            normalizer = RobustScaler(quantile_range=quantile_range)

        elif scaler == "StandardScaler":
            normalizer = StandardScaler()
        elif scaler == "MinMaxScaler":
            normalizer = MinMaxScaler()
        else:
            logger.warning("Unknown scaler, default to StandardScaler")
            normalizer = StandardScaler()

        normalizer.fit(self.train_data)
        self.normalizer = normalizer
        self.train_data = pd.DataFrame(
            normalizer.transform(self.train_data), columns=normalizer.feature_names_in_
        )
        self.test_data = pd.DataFrame(
            normalizer.transform(self.test_data), columns=normalizer.feature_names_in_
        )

    def make_dataset(
        self,
        normalize=True,
        multi=True,
        onehot_encode=True,
        proportion_train=0.7,
        **kwargs,
    ):
        """
        Function to actually create the dataset

        :param *removed_classes: Strings, classes to remove if needed
        :param normalize: Boolean
        for normalizing data or not
        :param removed_classes: list
        containing some classes to remove
        See method remove_class for more info
        :param proportion: float
        between 0 and 1
        :param min_instances_per_class: int
        minimum number of instances kept per class
        Necessary to avoid having one/zero instances in some classes if proportion != 1
        :param proportion_train: float
        proportion that train dataset takes
        :param **kwargs: dict, used for self.remove_class
        See its doc for more info
        """
        self.get_data()
        self.clean(**kwargs)
        self.drop_labels()
        if not multi:
            self.binary_labels()

        if onehot_encode:
            self.onehot_encoding()
        self.train_test_split(proportion_train, **kwargs)
        if normalize:
            self.normalize_data(**kwargs)

    def get_nb_features(self):
        try:
            return self.nb_features
        except AttributeError:
            raise Exception("Something went wrong, no number of features attribute")

    def get_feature_names(self):
        try:
            return self.train_data.columns
        except AttributeError:
            raise Exception("Something went wrong, no feature names attribute")

    def get_categorical_feature_names(self):
        try:
            return self.categorical_features
        except AttributeError:
            raise Exception(
                "Something went wrong, no categorical feature names attribute"
            )

    def get_nb_classes(self):
        try:
            return self.nb_classes
        except AttributeError:
            raise Exception("Something went wrong, no number of classes")

    def get_nb_classes_train(self):
        try:
            return self.nb_classes_train
        except AttributeError:
            self.nb_classes_train = len(np.unique(self.train_labels))
            return self.nb_classes_train

    def get_classes_support(self):
        try:
            return self.class_support
        except AttributeError:
            raise Exception("Something went wrong, no class support")

    def get_train_classes_support(self):
        try:
            _, train_labels = self.get_train_dataset()
            return {
                unique: counts
                for unique, counts in zip(*np.unique(train_labels, return_counts=True))
            }
        except AttributeError:
            raise Exception("Something went wrong, no train labels")

    def get_test_classes_support(self):
        try:
            _, test_labels = self.get_test_dataset()
            return {
                unique: counts
                for unique, counts in zip(*np.unique(test_labels, return_counts=True))
            }
        except AttributeError:
            raise Exception("Something went wrong, no test labels")

    def get_labels_dict(self):
        try:
            return self.labels_dict
        except AttributeError:
            raise Exception("Something went wrong, no dict for label correspondance")

    def get_removed_classes(self):
        try:
            return self.removed_classes
        except AttributeError:
            return []

    def get_train_dataset(self):
        """
        Getter of the train dataset
        :returns: Tuple
        train set data and labels
        """
        try:
            return self.train_data.to_numpy(), self.train_labels.to_numpy()
        except AttributeError:
            raise Exception("Something went wrong, no train dataset")

    def get_test_dataset(self):
        """
        Getter of the test dataset
        :returns: Tuple
        test set data and labels
        """
        try:
            return self.test_data.to_numpy(), self.test_labels.to_numpy()
        except AttributeError:
            raise Exception("Something went wrong, no test dataset")

    def get_dataset(self):
        """
        Getter of the test dataset
        :returns: Tuple
        test set data and labels
        """
        train_data, train_labels = self.get_train_dataset()
        test_data, test_labels = self.get_test_dataset()
        return train_data, train_labels, test_data, test_labels

    def get_normalizer(self):
        try:
            return self.normalizer
        except AttributeError:
            return StandardScaler(with_mean=False, with_std=False)

    def get_train_dict(self):
        try:
            return self.train_labels_dict
        except AttributeError:
            try:
                d = self.get_labels_dict()
                self.train_labels_dict = {
                    x: i for x, i in d.items() if i in np.unique(self.train_labels)
                }
                return self.train_labels_dict
            except AttributeError:
                raise Exception("Something went wrong, no train dataset")

    def get_test_dict(self):
        try:
            return self.test_labels_dict
        except AttributeError:
            try:
                d = self.get_labels_dict()
                self.test_labels_dict = {
                    x: i for x, i in d.items() if i in np.unique(self.test_labels)
                }
                return self.test_labels_dict
            except AttributeError:
                raise Exception("Something went wrong, no test dataset")

    def write_to_file(self, dataset_name, save_csv=False):
        train_data, train_labels, test_data, test_labels = self.get_dataset()
        os.makedirs("processed_data/{}".format(dataset_name), exist_ok=True)
        with open("processed_data/{}/train_data.npy".format(dataset_name), "wb") as f:
            np.save(f, train_data)
        with open("processed_data/{}/train_labels.npy".format(dataset_name), "wb") as f:
            np.save(f, train_labels)
        with open("processed_data/{}/test_data.npy".format(dataset_name), "wb") as f:
            np.save(f, test_data)
        with open("processed_data/{}/test_labels.npy".format(dataset_name), "wb") as f:
            np.save(f, test_labels)
        with open("processed_data/{}/dataset_info".format(dataset_name), "wb") as f:
            dataset_info = {
                "nb_features": self.get_nb_features(),
                "feature_names": self.get_feature_names(),
                "categorical_features": self.get_categorical_feature_names(),
                "nb_classes": self.get_nb_classes(),
                "nb_classes_train": self.get_nb_classes_train(),
                "class_support": self.get_classes_support(),
                "train_support": self.get_train_classes_support(),
                "test_support": self.get_test_classes_support(),
                "labels_dict": self.get_labels_dict(),
                "train_labels_dict": self.get_train_dict(),
                "test_labels_dict": self.get_test_dict(),
                "normalizer": self.get_normalizer(),
            }
            logger.info("Dataset info:\n{}".format(dataset_info))
            pickle.dump(dataset_info, f)
        if save_csv:
            feat_names = self.get_feature_names()
            pd_train = pd.DataFrame(
                np.concatenate((train_data, train_labels.reshape(-1, 1)), axis=1),
                columns=feat_names + ["Labels"],
            )
            pd_train.to_csv(self.path + f"{dataset_name}_train.csv", index=False)
            pd_test = pd.DataFrame(
                np.concatenate((test_data, test_labels.reshape(-1, 1)), axis=1),
                columns=feat_names + ["Labels"],
            )
            pd_test.to_csv(self.path + f"{dataset_name}_test.csv", index=False)
            pd_info = pd.DataFrame(dataset_info)
            pd_info.to_csv(self.path + f"{dataset_name}_dataset_info.csv", index=False)


class TorchDataset(Dataset):
    """
    Simple class for custom getter and len methods
    """

    def __init__(self, data, labels):
        self.data = torch.from_numpy(data).float()
        self.targets = torch.from_numpy(labels).long()

    def __len__(self):
        return len(self.targets)

    def __getitem__(self, idx):
        return self.data[idx], self.targets[idx]
