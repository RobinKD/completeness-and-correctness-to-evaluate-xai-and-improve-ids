#!/usr/bin/env python3

import lime.lime_tabular as lt
import numpy as np
import plotly.graph_objects as go
import pyplugs
from aim import Figure
from perf_metrics import mcc
from plotly.subplots import make_subplots
from sklearn.exceptions import NotFittedError
from tqdm import tqdm

from explainers import logger
from explainers.explan_utils import (
    completeness_check,
    correctness_check,
    get_indices,
)


def save_figure(
    expl,
    inst_id,
    feature_names,
    feature_values,
    class_names,
    inst_class,
    top_labels,
    nb_features,
    available_labels,
    aim_runner,
    aim_ctx,
    title_suffix="",
):
    fig = make_subplots(
        rows=1,
        cols=(top_labels + 2),
        subplot_titles=(
            ["Prediction probabilities"]
            + [
                "Influence for Class {}".format(class_names[lab])
                for lab in available_labels
            ]
        ),
        horizontal_spacing=0.1,
        specs=[[{"type": "xy"} for _ in range(top_labels + 1)] + [{"type": "table"}]],
    )

    fig.add_trace(
        go.Bar(
            x=expl.predict_proba,
            y=class_names,
            text=[round(v, 2) for v in expl.predict_proba],
            orientation="h",
            marker_color=[
                "rgb(0,0,{})".format(255 - 255 * v / 1.2) for v in expl.predict_proba
            ],
        ),
        row=1,
        col=1,
    )
    expl_map = expl.as_map()
    for num_lab, lab in enumerate(available_labels[:top_labels]):
        features, feature_importances = zip(*expl_map[lab])
        features = list(reversed(features[:nb_features]))
        feature_importances = list(reversed(feature_importances[:nb_features]))
        colors = ["blue" for _ in feature_importances]
        fig.add_trace(
            go.Bar(
                x=feature_importances,
                y=[feature_names[f] for f in features],
                orientation="h",
                marker_color=colors,
            ),
            row=1,
            col=num_lab + 2,
        )
        fig.add_trace(
            go.Table(
                header=dict(values=["Feature", "Value"]),
                cells=dict(
                    values=[
                        [feature_names[f] for f in features],
                        [feature_values[f] for f in features],
                    ]
                ),
            ),
            row=1,
            col=top_labels + 2,
        )
        fig.update_layout(
            title_text=f"Explanation for instance {inst_id} of class {class_names[inst_class]}"
            + title_suffix,
            title_x=0.5,
            title_y=0.95,
            margin_t=200 if title_suffix != "" else 0,
            autosize=False,
            showlegend=False,
            width=500 * (top_labels + 2),
            height=800,
        )
        aim_runner.track(
            Figure(fig),
            "explanation_rndinst",
            context=aim_ctx,
        )


@pyplugs.register
def lime_explanation(
    train_data,
    train_labels,
    test_data,
    test_labels,
    label_dict,
    feature_names,
    learner,
    aim_runner,
    normalizer,
    num_features=5,
    top_labels=2,
    checks={},
    **kwargs,
):
    model = learner.get_model()

    if learner.model.__class__.__name__ not in ["KMeans", "LinearSVC"]:
        class_names = list(label_dict.keys())
        explainer = lt.LimeTabularExplainer(
            train_data,
            feature_names=feature_names,
            class_names=class_names,
            discretize_continuous=False,
        )

        try:
            feature_values = normalizer.inverse_transform(test_data)
        except NotFittedError:
            feature_values = test_data

        if checks:
            if checks.get("check_train", False):
                check_data = train_data
                check_labels = train_labels

                feature_values = normalizer.inverse_transform(train_data)

            else:
                check_data = test_data
                check_labels = test_labels

            if checks.get("check_all_classes", False):
                indices = get_indices(
                    checks.get("num_checks", 3),
                    test_labels,
                    label_dict,
                )
                idx = [(c, i) for c, c_idx in indices.items() for i in c_idx]
            elif checks.get("check_all_attacks", False):
                idx = [
                    (check_labels[i], i)
                    for i in range(len(check_labels))
                    if class_names[check_labels[i]] != "normal"
                ]
            else:
                idx = [
                    (check_labels[i], i)
                    for i in np.random.choice(
                        len(check_data), checks.get("num_checks", 10)
                    )
                ]
            med = np.median(train_data, axis=0).reshape((1, train_data.shape[1]))
            classes = np.unique([c for c, _ in idx])
            class_support = {c: sum((check_labels == c)) for c in classes}
            check_names = [
                "correctness",
                "completeness",
            ]

            if checks.get("all_checks", False):
                for check_name in check_names:
                    checks[check_name]["check"] = True
            todo_checks = [
                check_name
                for check_name in check_names
                if checks[check_name].get("check", False)
            ]
            if len(todo_checks) == 0:
                logger.info("All checks false, nothing to do")
            else:
                nb_features = (
                    [num_features]
                    if not checks.get("all_decreasing_feature_numbers", False)
                    else [
                        i
                        for i in range(num_features, 1, -1)
                        if 0 not in [i % n for n in range(2, i)]
                    ]  # Prime numbers below num_features
                )
                checks_passed = {
                    nbfeat: {check_name: {-1: []} for check_name in todo_checks}
                    for nbfeat in nb_features
                }
                preds = np.argmax(model.predict_proba(check_data), axis=1).reshape(-1)
                for c, inst in tqdm(idx):
                    expl = explainer.explain_instance(
                        check_data[inst],
                        model.predict_proba,
                        num_features=num_features,
                        top_labels=len(np.unique(check_labels)),
                    )

                    exp_map = expl.as_map()
                    full_features_all, full_feature_importances_all = [], []
                    for lab in expl.available_labels():
                        ff, ffi = zip(*exp_map[lab])
                        full_features_all.append(ff)
                        full_feature_importances_all.append(ffi)
                    for nbfeat in nb_features:
                        context = {
                            "explan_alg": "lime",
                            "class": class_names[c],
                            "explanation_checks": True,
                            "nb_features": nbfeat,
                            "check_results": {},
                        }
                        features = [ff[:nbfeat] for ff in full_features_all]
                        for check_name in todo_checks:
                            if check_name == "correctness":
                                check_result = correctness_check(
                                    model, check_data[inst], med, features
                                )
                            elif check_name == "completeness":
                                check_result = completeness_check(
                                    model, check_data[inst], med, features
                                )
                            else:
                                logger.error("Unkown XAI check, will give 0")
                                check_result = 0

                            if c not in checks_passed[nbfeat][check_name]:
                                checks_passed[nbfeat][check_name][c] = [check_result]
                            else:
                                checks_passed[nbfeat][check_name][c].append(
                                    check_result
                                )
                            if check_name in ["completeness", "correctness"]:
                                context["check_results"][check_name] = bool(
                                    check_result
                                )
                            checks_passed[nbfeat][check_name][-1].append(check_result)
                        if checks.get("print_explanations", False) and nbfeat == max(
                            nb_features
                        ):
                            title_suffix = "<br>" + "<br>".join(
                                [
                                    f"{check_name}: {checks_passed[nbfeat][check_name][c][-1]}".capitalize()
                                    for check_name in todo_checks
                                ]
                            )
                            save_figure(
                                expl,
                                inst,
                                feature_names,
                                feature_values[inst],
                                class_names,
                                c,
                                1,
                                nbfeat,
                                expl.available_labels(),
                                aim_runner,
                                context,
                                title_suffix=title_suffix,
                            )
                for nbfeat in nb_features:
                    logger.info(f"For {nbfeat} features:")
                    for check_name in checks_passed[nbfeat].keys():
                        if checks.get("check_all_classes", False) or checks.get(
                            "check_all_attacks", False
                        ):
                            res_all_weighted = 0
                            for key in checks_passed[nbfeat][check_name].keys():
                                if key != -1:
                                    res = (
                                        sum(checks_passed[nbfeat][check_name][key])
                                        / len(checks_passed[nbfeat][check_name][key])
                                        * 100
                                    )
                                    aim_runner.track(
                                        res,
                                        name=f"{check_name} check".capitalize(),
                                        context={
                                            "XAI algo": "lime",
                                            "class": class_names[key],
                                        },
                                        step=nbfeat,
                                    )
                                    logger.info(
                                        f"{check_name} checks (class {class_names[key]}) passed: {res:.3f}%".capitalize()
                                    )
                                    res_all_weighted += (
                                        res
                                        * class_support[key]
                                        / sum(class_support.values())
                                    )
                            aim_runner.track(
                                res_all_weighted,
                                name=f"{check_name} check (weighted)".capitalize(),
                                context={
                                    "XAI algo": "lime",
                                },
                                step=nbfeat,
                            )
                            logger.info(
                                f"{check_name} checks (weighted) passed: {res_all_weighted:.3f}%".capitalize()
                            )
                        res_all = (
                            sum(checks_passed[nbfeat][check_name][-1])
                            / len(checks_passed[nbfeat][check_name][-1])
                            * 100
                        )
                        aim_runner.track(
                            res_all,
                            name=f"{check_name} check".capitalize(),
                            context={
                                "XAI algo": "lime",
                            },
                            step=nbfeat,
                        )
                        logger.info(
                            f"{check_name} checks passed: {res_all:.3f}%".capitalize()
                        )
                    if checks.get("get_correlation", False):
                        classes, i = zip(*idx)
                        classes = np.array(classes)
                        preds = np.argmax(
                            model.predict_proba(np.array(check_data[i, :])), axis=1
                        )

                        for check_name in ["completeness", "correctness"]:
                            for c in np.unique(classes):
                                correct = (
                                    np.array(classes[classes == c])
                                    == preds[classes == c]
                                )
                                conf_mat = np.array(
                                    [
                                        [
                                            sum(
                                                (correct == 0)
                                                * (
                                                    np.array(
                                                        checks_passed[nbfeat][
                                                            check_name
                                                        ][c]
                                                    )
                                                    == 0
                                                )
                                            ),
                                            sum(
                                                (correct == 1)
                                                * (
                                                    np.array(
                                                        checks_passed[nbfeat][
                                                            check_name
                                                        ][c]
                                                    )
                                                    == 0
                                                )
                                            ),
                                        ],
                                        [
                                            sum(
                                                (correct == 0)
                                                * (
                                                    np.array(
                                                        checks_passed[nbfeat][
                                                            check_name
                                                        ][c]
                                                    )
                                                    == 1
                                                )
                                            ),
                                            sum(
                                                (correct == 1)
                                                * (
                                                    np.array(
                                                        checks_passed[nbfeat][
                                                            check_name
                                                        ][c]
                                                    )
                                                    == 1
                                                )
                                            ),
                                        ],
                                    ]
                                )
                                correl = mcc(conf_mat)
                                logger.info(
                                    f"{check_name} check and prediction error correlation ({nbfeat} features,"
                                    f" class {class_names[c]}) = {correl}".capitalize()
                                )
                                aim_runner.track(
                                    correl,
                                    name=f"{check_name} check and prediction error correlation".capitalize(),
                                    context={
                                        "XAI algo": "lime",
                                        "class": class_names[c],
                                    },
                                    step=nbfeat,
                                )
