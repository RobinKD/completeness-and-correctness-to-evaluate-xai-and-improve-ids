#!/usr/bin/env python3

import numpy as np


def get_indices(
    nb_indices,
    test_labels,
    label_dict,
):
    if nb_indices == 0:
        return {}
    rng = np.random.default_rng()
    cond = {c: (test_labels == c) for c in list(label_dict.values())}
    cond = {key: val.nonzero()[0] for key, val in cond.items()}
    indices = {
        key: val
        if len(val) < nb_indices
        else rng.choice(val, nb_indices, replace=False)
        for key, val in cond.items()
    }

    return indices


def completeness_check(model, instance, median_values, important_features):
    """
    Because I am using the median, it might be biased when median is actually close
    to the initial value
    Might be worth it finding a better way to "erase" features
    Or just keep it in mind as a limitation
    """
    probas = model.predict_proba(instance.reshape(1, -1)).reshape(-1)
    if len(important_features) == 0:
        return np.argmax(probas)
    pred = np.argmax(probas)
    median_probas = model.predict_proba(median_values.reshape(1, -1)).reshape(-1)
    median_pred = np.argmax(median_probas)

    incomplete_preds = []
    for label, features in enumerate(important_features):
        incomplete_instance = np.copy(median_values).reshape(-1)
        incomplete_instance[list(features)] = np.copy(instance).reshape(-1)[
            list(features)
        ]
        incomplete_probas = model.predict_proba(
            incomplete_instance.reshape(1, -1)
        ).reshape(-1)
        incomplete_preds.append(np.argmax(incomplete_probas))

    if incomplete_preds[pred] == pred and pred != median_pred:
        return True
    else:
        return False


def correctness_check(model, instance, median_values, important_features):
    """
    Idea behind is that removing important features should change the prediction
    Same limitations as completeness check
    """
    probas = model.predict_proba(instance.reshape(1, -1)).reshape(-1)
    if len(important_features) == 0:
        return np.argmax(probas)
    previous_probas = np.copy(probas).reshape(-1)
    max_divide = [np.zeros_like(probas) for _ in important_features]
    for i in range(len(important_features)):
        max_divide[i][probas < 0.05] = -1

    for label, features in enumerate(important_features):
        copy_median = np.copy(median_values).reshape(-1)
        incomplete_instance = np.copy(instance)

        for _, imp_feature in enumerate(features):
            incomplete_instance[imp_feature] = copy_median[imp_feature]
            incomplete_probas = model.predict_proba(
                incomplete_instance.reshape(1, -1)
            ).reshape(-1)

            max_divide[label][max_divide[label] != -1] = np.max(
                np.vstack(
                    (
                        previous_probas[max_divide[label] != -1]
                        / np.where(
                            incomplete_probas[max_divide[label] != -1] > 0.01,
                            incomplete_probas[max_divide[label] != -1],
                            0.01,
                        ),
                        max_divide[label][max_divide[label] != -1],
                    )
                ),
                axis=0,
            )

            previous_probas = incomplete_probas
    max_divide_class = [max_divide[i][i] for i in range(len(important_features))]
    pred_median = np.argmax(model.predict_proba(median_values.reshape(1, -1)), axis=1)[
        0
    ]
    max_divide_class[pred_median] -= 1  # Base score is generally 1
    if max(max_divide_class) > 0 and np.argmax(max_divide_class) == np.argmax(probas):
        return True
    else:
        return False
