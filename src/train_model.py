#!/usr/bin/env python3

import os
from aim.sdk.base_run import MissingRunError
import dvc.api
import logging
import pickle
import numpy as np
from aim import Run

from make_aim import make_aim

logger = logging.getLogger("dvc")
logger.setLevel(10)


def train_model(params):
    dataset_name = params["dataset"]["name"]

    with open("processed_data/{}/dataset_info".format(dataset_name), "rb") as f:
        dataset_info = pickle.load(f)
    with open("processed_data/{}/train_data.npy".format(dataset_name), "rb") as f:
        train_data = np.load(f)
    with open("processed_data/{}/train_labels.npy".format(dataset_name), "rb") as f:
        train_labels = np.load(f)

    with open("untrained_model.pkl", "rb") as f:
        model = pickle.load(f)

    train_cvss = None
    if params["dataset"]["parameters"]["with_cve"]:
        with open("processed_data/{}/train_cvss".format(dataset_name), "rb") as f:
            train_cvss = pickle.load(f)

    try:
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    except MissingRunError:
        make_aim(params)
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
    logger.info("Current run hash: {}".format(aim_runner.hash))

    os.makedirs("save_experiments/train", exist_ok=True)
    if params["model"]["type"] == "DeepLearner":
        model.set_ckpt_path("save_experiments/trained_model")
    model.train(
        X=train_data,
        y=train_labels,
        label_dict=dataset_info["train_labels_dict"],
        aim_runner=aim_runner,
        train_cvss=train_cvss,
    )

    os.makedirs("save_experiments/trained_model", exist_ok=True)
    model.save_model("save_experiments/trained_model")


if __name__ == "__main__":
    params = dvc.api.params_show(stages="train_model")

    train_model(params)
