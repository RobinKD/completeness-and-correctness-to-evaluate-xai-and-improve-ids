#!/usr/bin/env python3

import logging
import os
import pickle
import warnings

from sklearn.metrics import confusion_matrix

warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")

import dvc.api
import lime.lime_tabular as lt
import numpy as np
import shap
from aim import Image, Run, Text
from aim.sdk.base_run import MissingRunError
from tqdm import tqdm

from make_aim import make_aim
from utils import save_cfm

logger = logging.getLogger("dvc")
logger.setLevel(10)


def completeness_change(model, instance, median_values, important_features):
    """
    Returns the class that has the biggest increase / smallest decrease
    by keeping only relevant features.
    Only returns classes that had an initial proba > 0.05
    """
    probas = model.predict_proba(instance.reshape(1, -1)).reshape(-1)
    if len(important_features) == 0:
        return np.argmax(probas)
    pred = np.argmax(probas)
    median_probas = model.predict_proba(median_values.reshape(1, -1)).reshape(-1)
    median_pred = np.argmax(median_probas)
    incomplete_preds = []
    for label, features in enumerate(important_features):
        incomplete_instance = np.copy(median_values).reshape(-1)
        incomplete_instance[list(features)] = np.copy(instance).reshape(-1)[
            list(features)
        ]
        incomplete_probas = model.predict_proba(
            incomplete_instance.reshape(1, -1)
        ).reshape(-1)
        incomplete_preds.append(np.argmax(incomplete_probas))

    if incomplete_preds[pred] == pred and pred != median_pred:
        return True
    else:
        return False


def correctness_change(model, instance, median_values, important_features, train_cfm):
    """
    Returns the class that has the biggest decrease
    by keeping only irrelevant features.
    Only returns classes that had an initial proba > 0.05
    """
    probas = model.predict_proba(instance.reshape(1, -1)).reshape(-1)
    if len(important_features) == 0:
        return np.argmax(probas)
    previous_probas = np.copy(probas).reshape(-1)
    pred = np.argmax(probas)
    max_divide = [np.zeros_like(probas) for _ in important_features]
    for i in range(len(important_features)):
        max_divide[i][probas < 0.05] = -1
        max_divide[i][train_cfm[pred] == 0] = -1

    for label, features in enumerate(important_features):
        copy_median = np.copy(median_values).reshape(-1)
        incomplete_instance = np.copy(instance)

        for _, imp_feature in enumerate(features):
            incomplete_instance[imp_feature] = copy_median[imp_feature]
            incomplete_probas = model.predict_proba(
                incomplete_instance.reshape(1, -1)
            ).reshape(-1)

            max_divide[label][max_divide[label] != -1] = np.max(
                np.vstack(
                    (
                        previous_probas[max_divide[label] != -1]
                        / np.where(
                            incomplete_probas[max_divide[label] != -1] > 0.01,
                            incomplete_probas[max_divide[label] != -1],
                            0.01,
                        ),
                        max_divide[label][max_divide[label] != -1],
                    )
                ),
                axis=0,
            )

            previous_probas = incomplete_probas
    max_divide_class = [max_divide[i][i] for i in range(len(important_features))]
    pred_median = np.argmax(model.predict_proba(median_values.reshape(1, -1)), axis=1)[
        0
    ]
    max_divide_class[pred_median] -= 1  # Base score is generally 1
    if max(max_divide_class) > 0 and np.argmax(max_divide_class) == np.argmax(probas):
        return True
    else:
        return False


def test_model(params):
    dataset_name = params["dataset"]["name"]

    with open("processed_data/{}/dataset_info".format(dataset_name), "rb") as f:
        dataset_info = pickle.load(f)
    with open("processed_data/{}/train_data.npy".format(dataset_name), "rb") as f:
        train_data = np.load(f)
    with open("processed_data/{}/train_labels.npy".format(dataset_name), "rb") as f:
        train_labels = np.load(f)
    with open("processed_data/{}/test_data.npy".format(dataset_name), "rb") as f:
        test_data = np.load(f)
    with open("processed_data/{}/test_labels.npy".format(dataset_name), "rb") as f:
        test_labels = np.load(f)

    try:
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)
        logger.info("Current run hash: {}".format(aim_runner.hash))
    except MissingRunError:
        make_aim(params)
        with open("current_aim_run", "r") as f:
            run_hash = f.read()
        aim_runner = Run(run_hash=run_hash)

    if os.environ.get("DVC_EXP_NAME", "unknown") not in aim_runner["dvc_exp"]:
        aim_runner["dvc_exp"].append(os.environ.get("DVC_EXP_NAME", "unknown"))

    with open("untrained_model.pkl", "rb") as f:
        learner = pickle.load(f)

    learner.load_model("save_experiments/trained_model")
    model = learner.get_model()

    train_preds = np.argmax(model.predict_proba(train_data), axis=1)
    train_cfm = confusion_matrix(train_labels, train_preds)

    xai_test_threshold = [i for i in range(5, 50)]
    threshold_divider = (max(xai_test_threshold) + 1) * 2
    xai_num_features_correctness = params["test_with_xai"]["num_features_correctness"]
    xai_num_features_completeness = params["test_with_xai"]["num_features_completeness"]
    feature_names = dataset_info["feature_names"]
    class_names = list(dataset_info["labels_dict"].keys())
    print(f"Class names: {[(i, cn) for i, cn in enumerate(class_names)]}")
    med = np.median(train_data, axis=0).reshape((1, train_data.shape[1]))
    pred_median = np.argmax(model.predict_proba(med).reshape(-1))
    support_classes = np.array([sum(test_labels == c) for c in np.unique(test_labels)])

    if learner.__class__.__name__ == "DeepLearner":
        y_pred = model.predict_proba(test_data)
    else:
        logger.error("Unknown model class")
        raise

    saved_features = {}
    explainer_correctness = {}
    explainer_completeness = {}

    for threshold in xai_test_threshold:
        above_threshold = []
        logger.info(f"Threshold: {threshold / threshold_divider}")
        if params["explanation"]["algo"] == "lime":
            explainer = lt.LimeTabularExplainer(
                train_data,
                feature_names=feature_names,
                class_names=class_names,
                discretize_continuous=False,
            )
            for inst, _ in enumerate(tqdm(test_labels)):
                if np.sort(y_pred[inst])[-2] > (threshold / threshold_divider):
                    above_threshold.append(inst)
                    if inst in saved_features:
                        features = saved_features[inst]
                    else:
                        expl = explainer.explain_instance(
                            test_data[inst],
                            model.predict_proba,
                            num_features=max(
                                xai_num_features_correctness,
                                xai_num_features_completeness,
                            ),
                            top_labels=len(class_names),
                        )

                        exp_map = expl.as_map()
                        features = {
                            "correctness": [[] for _ in expl.available_labels()],
                            "completeness": [[] for _ in expl.available_labels()],
                        }
                        for lab in expl.available_labels():
                            full_features, _ = zip(*exp_map[lab])
                            features["correctness"][lab] = full_features[
                                :xai_num_features_correctness
                            ]

                            features["completeness"][lab] = full_features[
                                :xai_num_features_completeness
                            ]

                        saved_features[inst] = features
                    label = test_labels[inst]

                    if inst not in explainer_correctness:
                        explainer_correctness[inst] = correctness_change(
                            model,
                            test_data[inst],
                            med,
                            features["correctness"],
                            train_cfm,
                        )
                    if inst not in explainer_completeness:
                        explainer_completeness[inst] = completeness_change(
                            model,
                            test_data[inst],
                            med,
                            features["completeness"],
                        )

        res = {
            c: {
                "need_investigation": 0,
            }
            for c in np.unique(test_labels)
        }

        new_preds = np.argmax(y_pred, axis=1)
        preds_certainty = np.array([[-1] * len(test_labels), [-1] * len(test_labels)])
        above_threshold_per_class = [0 for _ in class_names]
        for inst, pred in enumerate(tqdm(y_pred)):
            if inst in above_threshold:
                label = test_labels[inst]
                class_preds = np.argsort(pred)
                first_pred = class_preds[-1]

                above_threshold_per_class[first_pred] += 1
                new_pred = first_pred

                corr_val = explainer_correctness[inst]
                comp_val = explainer_completeness[inst]

                if not corr_val or not comp_val:
                    new_pred = label
                    res[first_pred]["need_investigation"] += 1

                new_preds[inst] = new_pred

        for c in res:
            aim_runner.track(
                res[c]["need_investigation"],
                "Human investigation needed",
                context={
                    "class": class_names[c],
                    "explan_algo": params["explanation"]["algo"],
                },
                step=threshold,
            )

        conf_mat = confusion_matrix(test_labels, new_preds)
        aim_runner.track(
            Text(f"{conf_mat}"),
            name="Full cfm",
            context={
                "explan_algo": params["explanation"]["algo"],
            },
            step=threshold,
        )

        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        aim_runner.track(
            Image(save_cfm(norm_conf_mat, dataset_info["labels_dict"])),
            "Normalized cfm",
            context={
                "explan_algo": params["explanation"]["algo"],
            },
            step=threshold,
        )
        for c in res:
            aim_runner.track(
                norm_conf_mat[c][c],
                "Accuracy",
                context={
                    "class": class_names[c],
                    "explan_algo": params["explanation"]["algo"],
                },
                step=threshold,
            )


if __name__ == "__main__":
    params = dvc.api.params_show(
        stages=["test_with_explan_model", "make_aim", "explain_model"]
    )

    if params["xai_test_improvements"]:
        test_model(params)
