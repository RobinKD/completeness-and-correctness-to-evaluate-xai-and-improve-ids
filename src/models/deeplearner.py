#!/usr/bin/env python3

import os
import shutil
import json
import pyplugs
import logging
import torch
import torch.nn as nn
import numpy as np
import pandas as pd
from utils import (
    get_perclass_log,
    get_acc,
    save_cfm,
    save_hist,
    MyJsonEncoder,
)
from aim import Image, Text
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.metrics import confusion_matrix
from models.learner import Learner
from perf_metrics import macro_average, micro_average
from datasets.basic_dataset import TorchDataset
from torch.utils.data import DataLoader
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping, Callback

logger = logging.getLogger("dvc")
logger.setLevel(10)


class TrackResultsCallback(Callback):
    def __init__(
        self,
        metrics,
        label_dict,
        aim_runner,
        figure_title=None,
        image_format="png",
        **kwargs,
    ):
        self.metrics = metrics
        self.label_dict = label_dict
        self.aim_runner = aim_runner
        self.figure_title = figure_title
        self.image_format = image_format
        self.best_epoch = {step: "0" for step in ["train", "validation", "test"]}
        if "out_path" in kwargs:
            self.out_path = kwargs["out_path"]

    def stack_output(self, outputs):
        losses = [o["loss"].cpu().detach() for o in outputs]
        losses = np.sum(losses) / len(losses)
        labels = torch.cat([o["labels"].cpu() for o in outputs]).numpy()
        preds = torch.cat([o["preds"].cpu() for o in outputs]).numpy()
        return {"loss": losses, "labels": labels, "preds": preds}

    def on_train_epoch_end(self, trainer, pl_module):
        train_results = self.stack_output(pl_module.training_step_outputs)
        self.save_results(
            train_results,
            trainer.current_epoch,
            step="train",
        )
        pl_module.training_step_outputs.clear()

    def on_validation_epoch_end(self, trainer, pl_module):
        validation_results = self.stack_output(pl_module.validation_step_outputs)
        self.save_results(
            validation_results,
            trainer.current_epoch,
            step="validation",
        )
        pl_module.validation_step_outputs.clear()

    def on_test_epoch_end(self, trainer, pl_module):
        test_results = self.stack_output(pl_module.test_step_outputs)
        self.save_results(
            test_results,
            trainer.current_epoch,
            step="test",
        )
        pl_module.test_step_outputs.clear()

    def save_results(
        self,
        epoch_results,
        epoch,
        step="test",
    ):
        results = {}
        loss = epoch_results["loss"]
        labels = epoch_results["labels"]
        preds = epoch_results["preds"]
        self.aim_runner.track(
            value=loss,
            name="Loss",
            context={"subset": step},
            epoch=epoch,
        )
        label_dict = {k: v for k, v in self.label_dict.items() if v in labels}
        conf_mat = confusion_matrix(labels, preds)
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["labels_dict"] = label_dict
        results["conf_mat"] = conf_mat.tolist()
        results["normalized_cm"] = norm_conf_mat.tolist()
        for k, f in self.metrics.items():
            res = f(conf_mat)
            if k not in ["Accuracy"]:
                results[k] = get_perclass_log(res, label_dict)
            else:
                results[k] = res
            if k not in ["Support", "Accuracy" "TP", "TN"]:
                results["macro_" + k] = macro_average(res)
                results["micro_" + k] = micro_average(res, conf_mat)
            for k in results:
                if "macro" in k or "micro" in k or k in ["Accuracy"]:
                    self.aim_runner.track(
                        value=results[k],
                        name="{}".format(k),
                        context={"subset": step},
                        epoch=epoch,
                    )
                elif k in [
                    "TPR",
                    "TNR",
                    "PPV",
                    "NPV",
                    "F1",
                ]:
                    for c in results[k]:
                        self.aim_runner.track(
                            results[k][c],
                            name="{}".format(k),
                            context={"subset": step, "class": c},
                            epoch=epoch,
                        )
        full_path = "save_experiments/{}/{}_full_results.json".format(
            "test" if step == "test" else "train", step
        )
        try:
            with open(full_path, "r") as f:
                saved_results = json.load(f)
        except FileNotFoundError:
            saved_results = {}

        with open(full_path, "w") as f:
            saved_results[str(epoch)] = {
                "Epoch": epoch,
                "Loss": loss,
                **results,
            }
            f.write(json.dumps(saved_results, indent=4, cls=MyJsonEncoder))

        if (
            self.best_epoch[step] in saved_results
            and saved_results[str(epoch)]["Loss"]
            <= saved_results[self.best_epoch[step]]["Loss"]
        ):
            self.best_epoch[step] = str(epoch)
            with open("save_experiments/{}_results.json".format(step), "w") as f:
                # Save only best epoch
                condensed_res = {
                    k: v
                    for k, v in saved_results[self.best_epoch[step]].items()
                    if "macro" in k
                    or "micro" in k
                    or k in ["Accuracy", "Epoch", "Loss"]
                }
                f.write(json.dumps(condensed_res, indent=4, cls=MyJsonEncoder))

        if step in ["validation", "test"]:
            figs = {}
            self.aim_runner.track(
                Text(f"{results['conf_mat']}"),
                name="Full cfm",
                context={"subset": step},
                epoch=epoch,
            )
            figs["normalized_cm"] = save_cfm(
                results["normalized_cm"], label_dict, figure_title=self.figure_title
            )
            for k in self.metrics:
                if k not in ["Support", "Accuracy", "TP", "TN"]:
                    figs["{}_hist".format(k)] = save_hist(
                        results["macro_" + k],
                        results["micro_" + k],
                        results[k],
                        k,
                        figure_title=self.figure_title,
                    )
            for name, fig in figs.items():
                if self.image_format == "svg":
                    self.aim_runner.track(
                        Image(fig, format="png"),
                        name=name,
                        context={"subset": step},
                        epoch=epoch,
                    )
                    try:
                        os.makedirs(self.out_path, exist_ok=True)
                        fig.savefig(
                            "{}/{}.svg".format(self.out_path, name), format="svg"
                        )
                    except KeyError:
                        logger.error("Save path undefined")
                else:
                    self.aim_runner.track(
                        Image(fig, format=self.image_format),
                        name=name,
                        context={"subset": step},
                        epoch=epoch,
                    )


def get_loss_weight(weight_type, class_support):
    if class_support is None:
        logger.warning("Class support is None")
        return None
    else:
        class_support = np.fromiter(class_support.values(), dtype=int)
        if weight_type == "class weights":
            return (
                1
                / torch.tensor([c / sum(class_support) for c in class_support]).float()
            )
        if weight_type == "log class weights":
            return (
                1
                / torch.tensor(
                    np.log([c / sum(class_support) for c in class_support])
                ).float()
            )
        else:
            logger.warning("Incorrect loss weight type")
            return None


class DeepNN(nn.Module):
    def __init__(self, *layer_sizes):
        super().__init__()
        layer_list = []
        self.nb_hidden = len(layer_sizes) - 2
        for i in range(self.nb_hidden + 1):
            input_size = layer_sizes[i]
            curr_size = layer_sizes[i + 1]
            layer_list.append(nn.Linear(input_size, curr_size))
            if i < self.nb_hidden:
                layer_list.append(nn.ReLU(inplace=False))
        self.net = nn.Sequential(*layer_list)

    def forward(self, x):
        return self.net(x)

    def predict_proba(self, x):
        """
        For compatibility with lime and sklearn models
        """
        self.net.to(device=torch.device("cpu"))
        if type(x) == type(pd.DataFrame([1])):
            x = x.to_numpy()
        if type(x) == type(np.zeros(1)):
            x = torch.from_numpy(x).float()
        return nn.functional.softmax(self.forward(x), dim=1).detach().numpy()

    def last_hidden_rep(self, x):
        """
        Returns the representation of the last hidden layer in the network,
        before ReLU activation

        :param x: array-like, input data
        :returns: array-like
        """
        return {"last_hidden": self.net[:-2](x).detach().numpy()}

    def hidden_rep(self, x, layer):
        """
        Returns the representation at the hidden layer specified by parameter

        :param x: array-like, input data
        :param layer: int, the hidden layer
        :returns: array-like
        """
        if not type(1) == type(layer):
            raise Exception("Should be an int")
        if not layer * 2 - 1 >= self.nb_hidden * 2 - 1:
            return {
                "hidden_" + str(layer): self.net[: layer * 2 - 1](x).detach().numpy()
            }
        else:
            if layer * 2 - 1 > self.nb_hidden * 2 - 1:
                logger.warning("Index too high, you will get the last hidden layer")
            return {"last_hidden": self.net[:-2](x).detach().numpy()}

    def all_hidden_rep(self, x):
        """
        Return a list of all hidden representations

        :param x: array-like, input data
        :returns: list of array-like
        """
        names = ["hidden_" + str(i) for i in range(1, self.nb_hidden + 1)]
        names[-1] = "last_hidden"
        return {
            name: hidden_rep
            for name, hidden_rep in zip(
                names,
                [
                    self.net[: layer * 2 + 1](x).detach().numpy()
                    for layer in range(self.nb_hidden)
                ],
            )
        }


class PlDeepNN(pl.LightningModule):
    def __init__(self, train_parameters, neuralnet, class_support=None):
        super().__init__()
        self.save_hyperparameters()

        self.model = neuralnet

        self.training_step_outputs = []
        self.validation_step_outputs = []
        self.test_step_outputs = []

        self.validation_frequency = train_parameters.get("validation_frequency", 5)
        self.max_epochs = train_parameters.get("nb_epochs", 100)
        try:
            loss_name = train_parameters["loss"]["name"]
            if loss_name == "CrossEntropyLoss":
                self.loss = nn.CrossEntropyLoss(
                    get_loss_weight(train_parameters["loss"]["weight"], class_support)
                )
        except KeyError as e:
            logger.warning(
                "Missing loss parameter {}\nDefault to basic CrossEntropy".format(e)
            )
            self.loss = nn.CrossEntropyLoss()

        try:
            self.lr_scheduler_name = train_parameters["optim"]["lr_scheduler"]["name"]
            self.lr_scheduler_parameters = train_parameters["optim"]["lr_scheduler"][
                "params"
            ]
        except KeyError as e:
            logger.warning(
                "Missing scheduler parameters: {}\nUsing ReduceLROnPlateau..."
            )
            self.lr_scheduler_name = "ReduceLROnPlateau"
            self.lr_scheduler_parameters = {}

        try:
            self.optimizer_name = train_parameters["optim"]["optimizer"]["name"]
            self.optimizer_parameters = train_parameters["optim"]["optimizer"]["params"]
        except KeyError as e:
            logger.warning(
                "Lacking optimizer parameters: {}\n Using Adam optimizer...".format(e)
            )
            self.optimizer_name = "Adam"
            self.optimizer_parameters = {}

        self.epoch_results = {"train": {}, "validation": {}, "test": {}}

    def evaluate(self, batch, stage=None):
        X, y = batch
        outs = self.model(X)
        loss = self.loss(outs, y)
        preds = torch.argmax(outs, dim=1)
        acc = get_acc(preds, y)

        if stage:
            self.log(f"{stage}_loss", loss, prog_bar=True)
            self.log(f"{stage}_acc", acc, prog_bar=True)

        return {"loss": loss, "labels": y, "preds": preds}

    def training_step(self, batch, batch_idx):
        res = self.evaluate(batch)
        self.training_step_outputs.append(res)
        return res

    def validation_step(self, batch, batch_idx):
        res = self.evaluate(batch, "val")
        self.validation_step_outputs.append(res)
        return res

    def test_step(self, batch, batch_idx):
        res = self.evaluate(batch, "test")
        self.test_step_outputs.append(res)
        return res

    def configure_optimizers(self):
        if self.optimizer_name == "Adam":
            optimizer = torch.optim.Adam(self.parameters(), **self.optimizer_parameters)
        elif self.optimizer_name == "AdamW":
            optimizer = torch.optim.AdamW(
                self.parameters(), **self.optimizer_parameters
            )
        else:
            logger.warning("Incorrect optimizer name, default to Adam")
            optimizer = torch.optim.Adam(self.parameters(), **self.optimizer_parameters)

        if self.lr_scheduler_name == "ReduceLROnPlateau":
            lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
                optimizer, **self.lr_scheduler_parameters
            )
        else:
            logger.warning("Incorrect lr scheduler name, default to ReduceLROnPlateau")
            lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)
        lr_scheduler_config = {
            # REQUIRED: The scheduler instance
            "scheduler": lr_scheduler,
            # The unit of the scheduler's step size, could also be 'step'.
            # 'epoch' updates the scheduler on epoch end whereas 'step'
            # updates it after a optimizer update.
            "interval": "epoch",
            # How many epochs/steps should pass between calls to
            # `scheduler.step()`. 1 corresponds to updating the learning
            # rate after every epoch/step.
            "frequency": self.validation_frequency,
            # Metric to to monitor for schedulers like `ReduceLROnPlateau`
            "monitor": "val_loss",
            # If set to `True`, will enforce that the value specified 'monitor'
            # is available when the scheduler is updated, thus stopping
            # training if not found. If set to `False`, it will only produce a warning
            "strict": True,
            # If using the `LearningRateMonitor` callback to monitor the
            # learning rate progress, this keyword can be used to specify
            # a custom logged name
            "name": None,
        }
        return {"optimizer": optimizer, "lr_scheduler": lr_scheduler_config}


@pyplugs.register
class DeepLearner(Learner):
    # TODO Document again
    def __init__(
        self,
        nb_features,
        nb_classes,
        class_support=None,
        random_state=43,
        model_params={},
        train_params={},
    ):
        """
        Initializes path, model, train and result attributes

        :param : list of ints,
        """
        super().__init__()
        pl.seed_everything(random_state)
        self.random_seed = random_state
        neuralnet = DeepNN(nb_features, *model_params["hidden_layers"], nb_classes)
        self.model = PlDeepNN(
            train_params,
            neuralnet,
            class_support=class_support,
        )
        self.train_batch_size = train_params.get("train_batch_size", 256)
        self.test_batch_size = train_params.get("test_batch_size", 128)

    def get_model(self):
        return self.model.model

    def set_ckpt_path(self, path):
        self.ckpt_path = path

    def train(
        self,
        X,
        y,
        label_dict,
        aim_runner,
        train_cvss=None,
    ):
        """
        Train routine of the DeepLearner
        """
        sss = StratifiedShuffleSplit(
            n_splits=1, test_size=0.1, random_state=self.random_seed
        )
        train_index, valid_index = next(sss.split(X, y))
        X_train, X_valid, y_train, y_valid = (
            X[train_index],
            X[valid_index],
            y[train_index],
            y[valid_index],
        )

        # Add one sample in valid for classes that are not present
        if len(np.unique(y_train)) != len(np.unique(y_valid)):
            not_present = np.where(~np.isin(np.unique(y_train), np.unique(y_valid)))[0]
            for c in not_present:
                one_sample_index = np.where(y_train == c)[0][0]
                X_valid = np.concatenate(
                    (X_valid, X_train[one_sample_index].reshape(1, -1)), axis=0
                )
                y_valid = np.concatenate(
                    (y_valid, y_train[one_sample_index].reshape(1)), axis=0
                )

        torch_train = TorchDataset(X_train, y_train)
        torch_valid = TorchDataset(X_valid, y_valid)

        loader_train = DataLoader(
            torch_train,
            batch_size=self.train_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
            sampler=None,
        )
        loader_valid = DataLoader(
            torch_valid,
            batch_size=self.test_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
            sampler=None,
        )

        checkpoint_callback = ModelCheckpoint(
            dirpath=self.ckpt_path,
            filename="model_checkpoint",
            save_top_k=1,
            monitor="val_loss",
        )

        earlystopping_callback = EarlyStopping("val_loss")

        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
        )

        trainer = pl.Trainer(
            accelerator="auto",
            # devices=1,
            logger=False,
            check_val_every_n_epoch=self.model.validation_frequency,
            num_sanity_val_steps=-1,
            callbacks=[
                checkpoint_callback,
                earlystopping_callback,
                tracking_metrics_callback,
            ],
            max_epochs=self.model.max_epochs,
        )
        trainer.fit(
            self.model, train_dataloaders=loader_train, val_dataloaders=loader_valid
        )

    def test(self, X, y, label_dict, aim_runner, test_cvss=None, **train_params):
        """
        Test routine of the DeepLearner

        :param dataset: DatasetLoader
        :param params_train: dict, see doc of method init_params for more info
        :param period_test: int, period to perform test/validation
        """
        torch_test = TorchDataset(X, y)
        loader_test = DataLoader(
            torch_test,
            batch_size=self.test_batch_size,
            shuffle=False,
            num_workers=4,
            pin_memory=True,
        )
        tracking_metrics_callback = TrackResultsCallback(
            self.metrics,
            label_dict,
            aim_runner,
            test_cvss_scores=test_cvss,
            **train_params,
        )
        trainer = pl.Trainer(
            accelerator="auto",
            devices=1,
            logger=False,
            callbacks=[
                tracking_metrics_callback,
            ],
        )
        trainer.test(self.model, loader_test)

    def save_model(self, path):
        shutil.move(path + "/model_checkpoint.ckpt", path + "/trained_model.ckpt")

    def load_model(self, path):
        try:
            self.model = PlDeepNN.load_from_checkpoint(path + "/trained_model.ckpt")
        except FileNotFoundError:
            logger.error("Failed to load model")
            raise Exception("No trained model")
