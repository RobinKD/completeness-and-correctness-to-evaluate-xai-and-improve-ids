#!/usr/bin/env python3

import os
import json
import pickle
import logging
import numpy as np
import perf_metrics as pm
from utils import get_perclass_log, save_cfm, save_hist, MyJsonEncoder
from perf_metrics import macro_average, micro_average
from sklearn.metrics import confusion_matrix
from aim import Image, Text


logger = logging.getLogger("dvc")
logger.setLevel(10)


class Learner:
    def __init__(self, with_cve=False):
        self.model = None
        self.metrics = {
            "Support": pm.support,
            "TP": pm.true_positive,
            "TN": pm.true_negative,
            "Accuracy": pm.accuracy,
            "TPR": pm.tpr,
            "TNR": pm.tnr,
            "PPV": pm.ppv,
            "NPV": pm.npv,
            "F1": pm.f1,
        }

    def get_model(self):
        return self.model

    def train(self):
        pass

    def test(self):
        pass

    def save_model(self, path):
        with open("{}/trained_model.pkl".format(path), "wb") as f:
            pickle.dump(self.model, f)

    def load_model(self, path):
        with open("{}/trained_model.pkl".format(path), "rb") as f:
            self.model = pickle.load(f)

    def save_results(
        self,
        y,
        y_pred,
        label_dict,
        aim_runner,
        step="test",
        figure_title=None,
        image_format="png",
        **kwargs,
    ):
        results = {}
        conf_mat = confusion_matrix(y, y_pred)
        norm_conf_mat = (conf_mat.T / conf_mat.sum(axis=1)).T
        results["labels_dict"] = label_dict
        results["conf_mat"] = conf_mat.tolist()
        results["normalized_cm"] = norm_conf_mat.tolist()
        for k, f in self.metrics.items():
            res = f(conf_mat)
            if k not in ["Accuracy"]:
                results[k] = get_perclass_log(res, label_dict)
            else:
                results[k] = res
            if k not in ["Support", "Accuracy", "TP", "TN"]:
                results["macro_" + k] = macro_average(res)
                results["micro_" + k] = micro_average(res, conf_mat)
            for k in results:
                if "macro" in k or "micro" in k or k in ["Accuracy"]:
                    aim_runner.track(
                        value=results[k],
                        name="{}".format(k),
                        context={"subset": step},
                    )
                elif k in [
                    "TPR",
                    "TNR",
                    "PPV",
                    "NPV",
                    "F1",
                ]:
                    for c in results[k]:
                        aim_runner.track(
                            results[k][c],
                            context={"subset": step, "class": c},
                            name="{}".format(k),
                        )
        with open("save_experiments/{0}/{0}_full_results.json".format(step), "w") as f:
            f.write(json.dumps(results, indent=4, cls=MyJsonEncoder))

        with open("save_experiments/{}_results.json".format(step), "w") as f:
            condensed_res = {
                k: v
                for k, v in results.items()
                if "macro" in k or "micro" in k or k in ["Accuracy"]
            }
            f.write(json.dumps(condensed_res, indent=4, cls=MyJsonEncoder))

        if step == "test":
            figs = {}
            aim_runner.track(
                Text(f"{results['conf_mat']}"),
                name="Full cfm",
            )
            figs["normalized_cm"] = save_cfm(
                results["normalized_cm"], label_dict, figure_title=figure_title
            )
            for k in self.metrics:
                if k not in ["Support", "Accuracy", "TP", "TN"]:
                    figs["{}_hist".format(k)] = save_hist(
                        results["macro_" + k],
                        results["micro_" + k],
                        results[k],
                        k,
                        figure_title=figure_title,
                    )
            for name, fig in figs.items():
                if image_format == "svg":
                    aim_runner.track(Image(fig, format="png"), name=name)
                    try:
                        os.makedirs(kwargs["out_path"], exist_ok=True)
                        fig.savefig(
                            "{}/{}.svg".format(kwargs["out_path"], name), format="svg"
                        )
                    except KeyError:
                        logger.error("Save path undefined")
                else:
                    aim_runner.track(Image(fig, format=image_format), name=name)
