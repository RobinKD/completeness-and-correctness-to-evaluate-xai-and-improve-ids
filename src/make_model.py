#!/usr/bin/env python3

import dvc.api
import logging
import pickle
import models as learners

logger = logging.getLogger("dvc")
logger.setLevel(10)


def make_model(params):
    dataset_name = params["dataset"]["name"]

    with open("processed_data/{}/dataset_info".format(dataset_name), "rb") as f:
        dataset_info = pickle.load(f)

    model_type = params["model"]["type"]
    if model_type == "DeepLearner":
        learner = learners.create_model(
            model_type.lower(),
            nb_features=dataset_info["nb_features"],
            nb_classes=dataset_info["nb_classes_train"],
            class_support=dataset_info["class_support"],
            random_state=params["random_seed"],
            model_params=params["model"]["parameters"],
            train_params=params["model"]["train_parameters"],
        )
    else:
        logger.error("Model type incorrect, creating dummy classifier...")

    with open("untrained_model.pkl", "wb") as f:
        pickle.dump(learner, f)


if __name__ == "__main__":
    params = dvc.api.params_show(stages="make_model")

    make_model(params)
