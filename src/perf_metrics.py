#!/usr/bin/env python3

import logging
import numpy as np
import pandas as pd
from torch import isnan

logger = logging.getLogger("dvc")
logger.setLevel(20)


def support(conf_mat):
    return conf_mat.sum(axis=1)


def true_positive(conf_mat):
    return conf_mat.diagonal()


def true_negative(conf_mat):
    return conf_mat.diagonal().sum() - conf_mat.diagonal()


def accuracy(conf_mat):
    return conf_mat.diagonal().sum() / conf_mat.sum().sum()


def tpr(conf_mat):
    return true_positive(conf_mat) / support(conf_mat)


def fpr(conf_mat):
    return 1 - tpr(conf_mat)


def tnr(conf_mat):
    negatives = conf_mat.sum() - conf_mat.sum(axis=1)
    return true_negative(conf_mat) / negatives


def fnr(conf_mat):
    return 1 - tnr(conf_mat)


def ppv(conf_mat):
    tp = true_positive(conf_mat)
    sum_preds = conf_mat.sum(axis=0)
    return np.divide(
        tp, sum_preds, out=np.zeros_like(tp, dtype=float), where=sum_preds != 0
    )


def npv(conf_mat):
    return true_negative(conf_mat) / (conf_mat.sum() - conf_mat.sum(axis=0))


def f1(conf_mat):
    num = 2 * ppv(conf_mat) * tpr(conf_mat)
    denom = ppv(conf_mat) + tpr(conf_mat)
    return np.divide(num, denom, out=np.zeros_like(num, dtype=float), where=denom != 0)


def macro_average(values):
    return values.mean()


def micro_average(values, conf_mat):
    sup = support(conf_mat)
    prop = sup / sup.sum()
    return (values * prop).sum()


def mcc(conf_mat):
    c = true_positive(conf_mat).sum()
    s = support(conf_mat).sum()
    t = conf_mat.sum(axis=1)
    p = conf_mat.sum(axis=0)
    denom1 = np.sqrt(s**2 - p.dot(p))
    denom2 = np.sqrt(s**2 - t.dot(t))
    if c == s:
        return 1.0
    elif denom1 == 0 or denom2 == 0:
        return 0
    else:
        return (c * s - t.dot(p)) / (denom1 * denom2)
