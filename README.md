# Completeness and Correctness to Evaluate XAI and Improve IDS

This git repository provides code and instructions to reproduce results from [this paper](https://doi.org/10.1145/3630050.3630177 "Explainability-based Metrics to Help Cyber Operators Find and Correct Misclassified Cyberattacks").

## Python environment

Both methods will install python packages along with nvidia and CUDA packages to run models on GPU.

Experiments were tested using NVIDIA driver *545.29.02* and CUDA version *12.2*.

### Flake & nix enabled system

Using flake.nix, you only need to do:

```
nix develop
pip install torch pytorch-lightning dvc lime shap numpy pytest joblib matplotlib seaborn click pyyaml graphviz pyqt5 plotly pyplugs aim ipython notebook==6.5.6
```

### Generic system

It is possible to create a python environment (preferably with Python 3.11) using the method of your choice. Required libraries are available in requirements.txt.

```
pip install -r requirements.txt
```

Or you can use any method of your choice, at your own risk.

## Obtain datasets

All three datasets are publicly available. If needed, the following presents instructions to download them.


### WADI

The dataset might be obtained by filling a form available at https://itrust.sutd.edu.sg/itrust-labs_datasets/


### UNSW-NB15

All four dataset files are available at https://research.unsw.edu.au/projects/unsw-nb15-dataset

### CIC-IDS2017

The full dataset used is the csv dataset and can be downloaded from https://www.unb.ca/cic/datasets/ids-2017.html (once again with a form).


## Manually Reproduce figures

The following blocks of code will use dvc and aim to run experiments and save raw results.
Datasets need to be in their own directory in a directory named *datasets* at the project root. Otherwise, python files (in *src/datasets/* directory) and yaml files (in *conf/dataset/* directory) related to datasets might need to be modified.

### Reproduce results for figure 1

```
dvc exp run -S dataset=WADI -S model=dnn_25 -S explanation=lime -S experiment_name="figure 1 - WADI" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.num_explan=0 -S explanation.checks.print_explanations=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50
dvc exp run -S dataset=UNSWNB15 -S model=dnn_25 -S explanation=lime -S experiment_name="figure 1 - UNSWNB15" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.num_explan=0 -S explanation.checks.print_explanations=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50
dvc exp run -S dataset=CICIDS2017 -S model=dnn_25 -S explanation=lime -S experiment_name="figure 1 - CICIDS2017" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.num_explan=0 -S explanation.checks.print_explanations=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50

```

### Reproduce results for figure 2 

```
dvc exp run -S dataset=WADI -S model=dnn_25 -S explanation=lime -S experiment_name="figure 2 - WADI" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50 -S explanation.checks.get_correlation=true
dvc exp run -S dataset=UNSWNB15 -S model=dnn_25 -S explanation=lime -S experiment_name="figure 2 - UNSWNB15" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50 -S explanation.checks.get_correlation=true
dvc exp run -S dataset=CICIDS2017 -S model=dnn_25 -S explanation=lime -S experiment_name="figure 2 - CICIDS2017" -S explanation.checks.correctness.check=true -S explanation.checks.completeness.check=true -S explanation.checks.check_all_attacks=true -S explanation.checks.get_correlation=true -S explanation.checks.all_decreasing_feature_numbers=true -S explanation.checks.check_train=true -S explanation.num_features=50 -S explanation.checks.get_correlation=true
```


### Reproduce results for figure 3

```
dvc exp run -S dataset=CICIDS2017 -S model=dnn_25 -S explanation=lime -S experiment_name="Figure 3 - CIC-IDS2017" -S explanation.num_explan=0 -S test_with_xai.num_features_correctness=30 -S test_with_xai.num_features_completeness=40 -S xai_test_improvements=true
```

### Get figures

In many cases, figures used in the article are directly visible using aim's UI and can be accessible by starting aim's web interface (`aim up` in a terminal).

Otherwise, it is possible to get all figures using the provided get_figures.py file. To do so, hash of run experiments need to be provided inside the file in order to read results from the experiments to create the figures. These hash are printed when the experiment is running, or can also be found in aim's web interface.

```
python get_figures.py
```


## Class Accuracy for the three datasets 

Results obtained by the trained Neural Network on the test set.
Values were truncated to the third decimal.

### WADI 

| Class         | Attack_1 | Attack_10 | Attack_13 | Attack_14 | Attack_15 | Attack_2 | Attack_3-4 | Attack_5 | Attack_6 | Attack_7 | Attack_8 | Attack_9 | Normal |
|---------------|-----------|------------|------------|------------|------------|-----------|-------------|-----------|-----------|-----------|-----------|-----------|--------|
| Accuracy (%) | 0.975     | 0.995      | 0.852      | 0.994      | 0.989      | 0.971     | 1.0         | 1.0       | 0.970     | 1.0       | 0.975     | 0.888     | 0.999  |


### UNSW-NB15 

| Class         | Analysis | Backdoor | DoS   | Exploits | Fuzzers | Generic | Reconnaissance | Shellcode | Worms | Normal |
|---------------|----------|----------|-------|----------|---------|---------|-----------------|-----------|-------|--------|
| Accuracy (%) | 0.028    | 0.034    | 0.176 | 0.884    | 0.456   | 0.982   | 0.740           | 0.821     | 0.0   | 0.996  |

### CIC-IDS2017

| Class         | Botnet | DDoS  | DoS GoldenEye | DoS Hulk | DoS Slowhttptest | DoS slowloris | FTP-Patator | Heartbleed | Infiltration | PortScan | SSH-Patator | Web Attack Brute Force | Web Attack SQL Injection | Web Attack XSS | Normal |
|---------------|--------|-------|-----------------------|------------------|------------------------------------|-----------------------|-------------|------------|--------------|----------|-------------|--------------------------------|----------------------------------|------------------------|--------|
| Accuracy (\%) | 0.630  | 0.999 | 0.985                 | 0.998            | 0.988                              | 0.978                 | 0.996       | 0.5        | 0.2          | 0.999    | 0.977       | 0.130                          | 0.0                              | 0.020                  | 0.996  |
