#!/usr/bin/env python3

import numpy as np
from aim import Run
from src.utils import generic_plot


def xai_acc_investigation(
    run_hash, dataset_name, xai_algo, has_title=True, has_legend=True, svg=False
):
    aim_run = Run(run_hash)
    steps = np.array([i + 5 for i in range(45)]) / 100
    values = {}
    for metric in aim_run.metrics():
        # print(f"{metric.name}")
        if (
            ("Accuracy" in metric.name or "investigation" in metric.name)
            and "class" in metric.context
            # and metric.context["explan_algo"] == xai_algo
        ):
            print(f"Context: {metric.context}")
            data = metric.data.numpy()
            print(
                f"""{metric.name}:
Steps: {data[0]}
Values: {data[1][0]}"""
            )
            if metric.name not in values:
                values[metric.name] = {}
            values[metric.name][metric.context["class"]] = data[1][0]

    for m in values:
        ylim = (None, None)
        values[m] = dict(sorted(values[m].items()))
        if "normal" in values[m]:
            values[m] = {
                **{c: values[m][c] for c in values[m] if c != "normal"},
                "normal": values[m]["normal"],
            }
            nl = "\n"
            dd = np.array(list(values[m].values()))
            print(
                f"Plot {m}:\n"
                f"Steps, {', '.join(map(lambda x: '$' + x[:1].upper() + x[1:] + '$', list(values[m].keys())))}\n"
                f"{nl.join([str(s) + ', ' + ', '.join(map(str, v)) for s, v in zip(steps, list(dd.T))])}"
            )
            generic_plot(
                steps,
                list(values[m].values()),
                list(values[m].keys()),
                f"figures/{'no_title_' if not has_title else ''}{'no_legend_' if not has_legend else ''}{xai_algo}_{dataset_name}_{'_'.join(m.split(' '))}",
                f"{m}",
                "Threshold value",
                f"{m}",
                ylim,
                has_title,
                has_legend,
                svg,
            )


def xai_coco_res(
    run_hash, dataset_name, xai_algo, has_title=True, has_legend=True, svg=False
):
    aim_run = Run(run_hash)
    steps_ok = [i for i in range(50, 2, -1) if 0 not in [i % n for n in range(2, i)]]
    values = {}
    steps = {}
    for metric in aim_run.metrics():
        # print(f"{metric.name}")
        if (
            "check" in metric.name
            and "class" in metric.context
            and metric.context["XAI algo"] == xai_algo
        ):
            print(f"Context: {metric.context}")
            data = metric.data.numpy()
            print(
                f"""{metric.name}:
Steps: {data[0]}
Values: {data[1][0]}"""
            )
            if metric.name not in values:
                values[metric.name] = {}
            values[metric.name][metric.context["class"]] = [
                d for s, d in zip(data[0], data[1][0]) if s in steps_ok
            ]
            steps[metric.name] = data[0]

    for m in values:
        ylim = (None, None)
        if "correlation" in m:
            ylim = (-1.05, 1.05)
        elif "check" in m:
            ylim = (-5, 105)
        values[m] = dict(sorted(values[m].items()))
        if "normal" in values[m]:
            values[m] = {
                **{c: values[m][c] for c in values[m] if c != "normal"},
                "normal": values[m]["normal"],
            }
        steps_kept = [s for s in steps[m] if s in steps_ok]
        nl = "\n"
        dd = np.array(list(values[m].values()))
        print(
            f"Plot {m}:\n"
            f"Steps, {', '.join(map(lambda x: '$' + x[:1].upper() + x[1:] + '$', list(values[m].keys())))}\n"
            f"{nl.join([str(s) + ', ' + ', '.join(map(str, v)) for s, v in zip(steps_kept, list(dd.T))])}"
        )
        generic_plot(
            steps_kept,
            list(values[m].values()),
            list(values[m].keys()),
            f"figures/{'no_title_' if not has_title else ''}{'no_legend_' if not has_legend else ''}{xai_algo}_{dataset_name}_{'_'.join(m.split(' '))}",
            f"{m}",
            "Number of features",
            f"{m} value",
            ylim,
            has_title,
            has_legend,
            svg,
        )


if __name__ == "__main__":
    # Lime completeness and correctness checks and correlation
    # xai_coco_res("3c07eda208af408684b87b61", "unsw", "lime")
    # xai_coco_res("fbaa86f4203f491cbd775295", "wadi", "lime")
    # xai_coco_res("b3ff9c76f4694a019159abe1", "cicids2017", "lime")

    # Lime prediction improvements and manual investigations required
    # xai_acc_investigation("735a5701a61e46a4b086992b", "wadi", "lime")
    # xai_acc_investigation("c6e55bc4a38b4f2fa8c9acf7", "unsw", "lime")
    # xai_acc_investigation(
    #     "caa8f540aa80455398f7b409", "cicids2017", "lime"
    # )

    pass
