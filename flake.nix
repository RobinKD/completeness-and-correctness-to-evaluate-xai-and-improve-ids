{
  description = "A basic nix + flake python dev env";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { nixpkgs, utils, self }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            allowUnfree = true;
            cudaSupport = true;
            cudaVersion = "12.2";
          };
        };
        typing_extensions_pypi = pkgs.python3Packages.buildPythonPackage rec {
          pname = "typing-extensions";
          version = "4.8.0";
          format = "wheel";
          src = pkgs.fetchurl {
            url =
              "https://files.pythonhosted.org/packages/24/21/7d397a4b7934ff4028987914ac1044d3b7d52712f30e2ac7a2ae5bc86dd0/typing_extensions-4.8.0-py3-none-any.whl";
            hash = "sha256-j5L8iAb5prZB6qUxjaMrRNQB76rA9meMm8RIujYF+qA=";
          };
        };
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            (python3.withPackages (ps:
              with ps; [
                # my_platformdirs
                pip
                virtualenv
                typing_extensions_pypi
              ]))
            cudaPackages_12_2.cudatoolkit

            # Run pip install torch pytorch-lightning dvc lime shap numpy pytest joblib matplotlib seaborn click pyyaml graphviz pyqt5 plotly pyplugs aim ipython notebook==6.5.6

          ];

          shellHook = ''
            # Tells pip to put packages into $PIP_PREFIX instead of the usual locations.
            # See https://pip.pypa.io/en/stable/user_guide/#environment-variables.
            export QT_QPA_PLATFORM_PLUGIN_PATH="${pkgs.qt5.qtbase.bin}/lib/qt-${pkgs.qt5.qtbase.version}/plugins";
            export PIP_PREFIX=$(pwd)/venv/pip_packages
            export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
            export PATH="$PIP_PREFIX/bin:$PATH"
            export CUDA_PATH=${pkgs.cudaPackages_12_2.cudatoolkit}
            export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib/:${pkgs.linuxPackages.nvidia_x11}/lib
            export EXTRA_LDFLAGS="-L/lib -L${pkgs.linuxPackages.nvidia_x11}/lib"
            export EXTRA_CCFLAGS="-I/usr/include"
            unset SOURCE_DATE_EPOCH
          '';
        };
      });
}
